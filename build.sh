#! /bin/bash
if [ "$1" = "init" ]; then
  npm install
  bower install
  ionic state reset
  ionic build ios
  ionic build ios
  ionic emulate ios
else
  ionic build ios
  ionic emulate ios
fi