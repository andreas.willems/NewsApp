/* jshint -W117, -W030 */
(function() {
  'use strict';

  angular
	.module('app')
	.config(routes);

  routes.$inject = ['$stateProvider', '$urlRouterProvider'];

  function routes($stateProvider, $urlRouterProvider) {
		$stateProvider

		.state('app', {
			url: '/app',
			abstract: true,
			templateUrl: 'app/menu/menu.html',
			controller: 'MenuController as menuCtrl'
		})

		.state('login', {
			url: '/',
			templateUrl: 'app/login/login.html',
			controller: 'LoginController as loginCtrl',
		})

		.state('app.listview', {
			url: '/list',
			views: {
				'menuContent': {
					templateUrl: 'app/list/list.html',
					controller: 'ListController as listCtrl',
					onEnter: function() {
						listCtrl.activate();
					}
				}
			}
		})

		.state('app.detail', {
			url: '/list/:id',
			views: {
				'menuContent': {
					templateUrl: 'app/detail/detail.html',
					controller: 'DetailController as detailCtrl'
				}
			}
		});

    $urlRouterProvider.otherwise('/');
  }

})();
