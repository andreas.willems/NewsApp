/* jshint -W117, -W030 */
describe('Test suite: List controller', function() {
  var $controller;
  var $scope;
  var scopeMock;
  var deferredRequest;
  var newsServiceMock;
  var News;
  var ionicLoadingMock;

  // Load the module for the list
  beforeEach(module('app.list'));

  // Instantiate the controller and mocks for every test
  beforeEach(inject(function(_$controller_, $injector, $rootScope, $q) {

    News = $injector.get('News');
    deferredRequest = $q.defer();

    // mock service
    newsServiceMock = {
      getAllNews: jasmine.createSpy('getAllNews')
        .and.returnValue(deferredRequest.promise),
      getNewsFromPage: jasmine.createSpy('getNewsFromPage')
        .and.returnValue(deferredRequest.promise),
      listen: jasmine.createSpy('listen')
    };

    // create ionicLoadingMock
    ionicLoadingMock = jasmine.createSpyObj('$ionicLoading', ['show', 'hide']);

    // create $scope object
    $scope = jasmine.createSpyObj('$scope', ['$broadcast', '$watch']);

    // initiate controller
    $controller = _$controller_('ListController', {
      'News': newsServiceMock,
      '$scope': $scope,
      '$ionicLoading': ionicLoadingMock
    });
  }));

  describe('The controller', function() {

    it ('should exist', function() {
      expect($controller).toBeDefined();
    });

    it('should define a method getNews', function() {
      expect($controller.getNews).toBeDefined();
    });

    it('should define a method resetPageCountAndGetNews', function() {
      expect($controller.resetPageCountAndGetNEws).toBeDefined();
    });

    it('should define a method onSuccess', function() {
      expect($controller.onSuccess).toBeDefined();
    });

    it('should define a method onError', function() {
      expect($controller.onError).toBeDefined();
    });

    it('should define a method onFinally', function() {
      expect($controller.onFinally).toBeDefined();
    });

    /*it('should define a method showFilterBar', function() {
      expect($controller.showFilterBar).toBeDefined();
    });*/

  });

  describe('#getNews()', function() {
    it('should make a call to News.getNewsFromPage', function() {
      $controller.getNews();
      expect(newsServiceMock.getNewsFromPage).toHaveBeenCalled();
    });
  });

  describe('#resetPageCountAndGetnews()', function() {
    it('should make a call to getNews', function() {
      $controller.resetPageCountAndGetNews();
      expect($controller.currentPage).toBe(1);
      expect($controller.nextPage).toBe(2);
      expect($controller.news.length).toBe(0);
    });
  });

  describe('#onSuccess()', function() {
    it('should change the value of vm.news', function() {
      $controller.onSuccess({data: {results: []}});
      expect($controller.news).toBeDefined();
      expect(Array.isArray($controller.news)).toBeTruthy();
    });
  });

  describe('#onError()', function() {
    it('should call $ionicLoading.show', function() {
      var response = {status: 500};
      $controller.onError(response);
      expect(ionicLoadingMock.show).toHaveBeenCalled();
    });
  });

  describe('#onFinally()', function() {
    it('should call $scope.$broadcast with "scroll.refreshComplete"', function() {
      $controller.onFinally();
      expect($scope.$broadcast).toHaveBeenCalledWith('scroll.refreshComplete');
    });

    it('should call $scope.$broadcast with "scroll.infiniteScrollComplete"', function() {
      $controller.onFinally();
      expect($scope.$broadcast).toHaveBeenCalledWith('scroll.infiniteScrollComplete');
    });
  });
});
