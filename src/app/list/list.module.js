(function() {
  'use strict';

  angular
	.module('app.list', [
	  'ionic',
    'news'
	]);
})();
