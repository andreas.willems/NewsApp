(function() {
  'use strict';

  angular
    .module('app.list')
    .filter('dateDayMonthYear', DateDayMonthYear);

  function DateDayMonthYear() {
    return function(input) {
      if (input) {
        var data = input.split(' ');
        var datum = data[0].split('-');
        var day = datum[2];
        var month = datum[1];
        var year = datum[0];
        return day + '.' + month + '.' + year;
      }
    };
  }

})();