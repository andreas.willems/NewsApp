(function() {
	'use strict';
	
	angular
    .module('app.logger')
    .factory('Logger', Logger);

  Logger.$inject = ['$http'];

  function Logger($http) {
	  var server = 'http://192.168.1.50:8000/api/logs';
	  var service = {
		  logError: logError
	  };
	  return service;
	  
	  function logError(message) {
		  
		  var url = server + '/errors';
		  var errorMessage = Date.now() + '_' + message; 
		  
		  $http
		  	.post(url, errorMessage)
			.then(function(response) {
				
			})
			.catch(function(response) {
				
			});
	  }
  }
})();