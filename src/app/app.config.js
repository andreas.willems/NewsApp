(function() {
  'use strict';

  angular
    .module('app')
    .config(configure);

    configure.$inject = ['$translateProvider'];

	function configure ($translateProvider) {
      $translateProvider.translations('en', {
        MENU: 'Menu',
        SORT: 'SORT',
        LANGUAGE: 'LANGUAGE',
        ENGLISH: 'english',
        GERMAN: 'german',
        OTHER: 'OTHER',
        LOGOUT: 'Logout',
        SIMILAR_NEWS: 'Similar news',
        FAVORITE: 'Favorite',
        VISIT_SITE: 'Visit website',
        SHARE: 'Share',
        INDUSTRIES: 'Industries',
        CONTEXT: 'Context',
        CATEGORY: 'Category'
      });
      $translateProvider.translations('de', {
        MENU: 'Menü',
        SORT: 'Sortierung',
        LANGUAGE: 'Sprache',
        ENGLISH: 'Englisch',
        GERMAN: 'Deutsch',
        OTHER: 'Sontiges',
        LOGOUT: 'Ausloggen',
        SIMILAR_NEWS: 'Ähnliche News',
        FAVORITE: 'Favorit',
        VISIT_SITE: 'Webseite besuchen',
        SHARE: 'Teilen',
        INDUSTRIES: 'Branchen',
        CONTEXT: 'Kontext',
        CATEGORY: 'Kategorie'
      });
      $translateProvider.preferredLanguage('en');
	}
})();
