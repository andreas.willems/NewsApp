(function() {
	'use strict';

	angular.module('app.detail', [
		'ionic',
		'ngCordova',
		'app.list',
		'news',
		'ksSwiper'
	]);
})();
