(function() {
  'use strict';

  angular
    .module('app.detail')
    .controller('DetailController', DetailController);

  DetailController.$inject = [
    '$stateParams', '$ionicLoading', 'News', '$scope',
    '$state', '$window', '$ionicActionSheet', '$cordovaActionSheet',
    '$ionicSlideBoxDelegate'
  ];

  function DetailController($stateParams, $ionicLoading, News, $scope,
                            $state, $window, $ionicActionSheet,
                            $cordovaActionSheet, $ionicSlideBoxDelegate) {

    var vm = this;
    vm.newsItem = {};
    vm.id = 1;
    vm.swiper = {};
    vm.onSuccess = onSuccess;
    vm.onError = onError;
    vm.onFinally = onFinally;
    vm.openLinkInBrowser = openLinkInBrowser;
    vm.onDeviceReady = onDeviceReady;
    vm.getNewsItem = getNewsItem;
    vm.getItemsIndustries = getItemsIndustries;
    vm.getItemsDomains = getItemsDomains;
    vm.getItemsCategory = getItemsCategory;
    vm.onReadySwiper = onReadySwiper;
    vm.toggleFavorite = toggleFavorite;
    vm.showActionSheet = showActionSheet;
    vm.updateSlider = updateSlider;
    vm.isFavorite = false;
    vm.favoriteStyle = 'ion-ios-star-outline';
    vm.industries = [];
    vm.domains = [];
    vm.category = '';

    $scope.$on('$ionicView.beforeEnter', function() {
      vm.getNewsItem($stateParams.id);
    });

    return vm;

    function toggleFavorite() {
      if (vm.isFavorite) {
        vm.isFavorite = false;
        vm.favoriteStyle = 'ion-ios-star-outline';
      } else {
        vm.isFavorite = true;
        vm.favoriteStyle = 'ion-ios-star';
      }
    }

    function updateSlider() {
      return $ionicSlideBoxDelegate.update();
    }

    function showActionSheet() {
      var options = {
        title: 'Share this news',
        buttonLabels: ['E-Mail', 'Facebook', 'Twitter'],
        addCancelButtonWithLabel: 'Cancel',
        androidEnableCancelButton : true,
        winphoneEnableCancelButton : true
      };

      $cordovaActionSheet
        .show(options)
        .then(function(btnIndex) {}, false);
    }

    function getNewsItem(id) {
      vm.id = id;
      $ionicLoading.show();
      News
        .getNewsById(id)
        .then(onSuccess, onError)
        .finally(onFinally);
    }

    function getItemsIndustries(id) {
      return News
        .getIndustriesForNewsWithId(id)
        .then(function(response) {
          vm.industries = response.data.results;
          vm.getItemsDomains(id);
        })
        .catch(function(response){ onError(response) });
    }

    function getItemsDomains(id) {
      return News
        .getDomainsForNewsWithId(id)
        .then(function(response) {
          vm.domains = response.data.results;
          vm.getItemsCategory(vm.newsItem.categoryId);
        })
        .catch(function(response){ onError(response) });
    }

    function getItemsCategory(id) {
      return News
        .getCategoryForNewsWithId(id)
        .then(function(response) {
          vm.category = response.data.translations[0].title;
        })
        .catch(function(response) { onError(response) });
    }

    function onSuccess(response) {
      vm.newsItem = response.data;
      vm.getItemsIndustries(vm.id);
      $ionicLoading.hide();
    }

    function onError(response) {
      $ionicLoading.show({
        template: 'Could not load news. Please try again later.',
        duration: 3000
      });
    }

    function onFinally() {
      $scope.$broadcast('scroll.refreshComplete');
    }

    function openLinkInBrowser() {
      document.addEventListener('deviceready', onDeviceReady, false);
    }

    function onDeviceReady() {
      $window.open(vm.newsItem.url, '_system', 'location=yes'); //status=yes
      return false;
    }
  }
})();
