/* jshint -W117, -W030 */
describe('Test suite: Detail controller', function() {
  var $controller;
  var stateParamsMock;
  var deferredRequest;
  var newsServiceMock;
  var ionicLoadingMock;
  var News;
  var scope;
  var state;
  var documentMock;

  // Load the module for the list
  beforeEach(module('app.detail'));

  // Instantiate the controller and mocks for every test
  beforeEach(inject(function(_$controller_, $injector, $rootScope, $q) {

    News = $injector.get('News');
    deferredRequest = $q.defer();

    // mock List service
    newsServiceMock = {
      getNewsById: jasmine.createSpy('getNewsById')
        .and.returnValue(deferredRequest.promise),
      getIndustriesForNewsWithId: jasmine.createSpy('getIndustriesForNewsWithId')
        .and.returnValue(deferredRequest.promise)
    };

    // create stateParamsMock object
    stateParamsMock = jasmine.createSpyObj('$stateParams', ['id']);

    // create ionicLoadingMock
    ionicLoadingMock = jasmine.createSpyObj('$ionicLoading', ['show', 'hide']);

    // create mock scope
    scope = jasmine.createSpyObj('$scope', ['$broadcast', '$on']);

    // create mock state
    state = jasmine.createSpyObj('$state', ['go']);

    // create mock document
    documentMock = jasmine.createSpyObj('document', ['addEventListener']);

    // initiate controller
    $controller = _$controller_('DetailController', {
      '$stateParams': stateParamsMock,
      '$ionicLoading': ionicLoadingMock,
      'News': newsServiceMock,
      '$scope': scope,
      '$state': state,
      'document': documentMock
    });
  }));

  describe('The controller', function() {

    it ('should exist', function() {
      expect($controller).toBeDefined();
    });

    it('should define a method getNewsItem', function() {
      expect($controller.getNewsItem).toBeDefined();
    });

    it('should define a method onSuccess', function() {
      expect($controller.onSuccess).toBeDefined();
    });

    it('should define a method onFinally', function() {
      expect($controller.onFinally).toBeDefined();
    });

    it('should define a method openLinkInBrowser', function() {
      expect($controller.openLinkInBrowser).toBeDefined();
    });

  });

  describe('#getNewsItem()', function() {
    it('should make a call to News.getNewsById', function() {
      $controller.getNewsItem();
      expect(newsServiceMock.getNewsById).toHaveBeenCalled();
    });
  });

  describe('#onSuccess()', function() {
    it('should change the value of vm.news', function() {
      $controller.onSuccess({data: {id: 1}});
      expect($controller.newsItem).toBeDefined();
      expect($controller.newsItem.id).toBe(1);
    });

    it('should call $ionicLoading.hide', function() {
      $controller.onSuccess({data: {id: 1}});
      expect(ionicLoadingMock.hide).toHaveBeenCalled();
    });
  });

  describe('#onError()', function() {
    it('should call $ionicLoading.show', function() {
      var response = {status: 500};
      $controller.onError(response);
      expect(ionicLoadingMock.show).toHaveBeenCalled();
    });
  });

  describe('#onFinally()', function() {
    it('should call $scope.$broadcast with "scroll.refreshComplete"', function() {
      $controller.onFinally();
      expect(scope.$broadcast).toHaveBeenCalledWith('scroll.refreshComplete');
    });
  });

  describe('#openLinkInBrowser()', function() {

  });

  describe('#onDeviceReady()', function() {
    /*it('should make a call to $state.go', function() {
      $controller.onDeviceReady();
      expect(state.go).toHaveBeenCalled();
    });*/
  });
});
