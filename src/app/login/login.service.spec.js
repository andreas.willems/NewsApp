/* jshint -W117, -W030 */
describe('Test suite: Authentication service', function() {
	var Authentication;

	// Load the module for the login
	beforeEach(module('app.login'));

	// Inject the service
	beforeEach(inject(function($injector) {
		Authentication = $injector.get('Authentication');
		$httpBackend = $injector.get('$httpBackend');
	}));

	describe('The service', function() {
		it('should exist', inject(function(Authentication) {
			expect(Authentication).toBeDefined();
		}));

		it('should define a method login', inject(function(Authentication) {
			expect(Authentication.login).toBeDefined();
		}));
	});

});
