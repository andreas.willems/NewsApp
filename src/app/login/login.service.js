(function() {
  'use strict';

  angular
  .module('app.login')
	.factory('Authentication', Authentication);

	Authentication.$inject = ['$http', '$ionicPopup', '$window'];

	function Authentication($http, $ionicPopup, $window) {
      
      var clientId = 'client_id=...';
      var clientSecret = 'client_secret=...';
      var redirectUri = 'redirect_uri=http://localhost/callback';
      var browserWindow;
      var requestToken;

      var vm = this;
      vm.performOauth = performOauth;
      vm.storeInLocalStorage = storeInLocalStorage;
      vm.removeFromLocalStorage = removeFromLocalStorage;
      vm.onSuccess = {};

      // return the service interface
      var service = {
        login: function(onSuccess) {
          //onSuccess();
          defineMethodStartsWith();
          vm.performOauth(onSuccess);
        },
        logout: function(onSuccess) {
          removeFromLocalStorage('access_token');
          onSuccess();
        }
      };
      return service;
      
      function storeInLocalStorage(key, value) {
        $window.localStorage.setItem(key, value);
      }
      
      function removeFromLocalStorage(key) {
        $window.localStorage.setItem(key, null);
        requestToken = null;
      }

      function defineMethodStartsWith() {
        if (typeof String.prototype.startsWith !== 'function') {
          /* jshint -W121*/
          String.prototype.startsWith = function (str){
            return this.indexOf(str) === 0;
          };
        }
      }

      function buildAuthUrl() {
        var url = '...';
        url += clientId + '&';
        url += clientSecret + '&';
        url += redirectUri + '&';
        url += 'response_type=code&access_type=offline&approval_prompt=force';
        return url;
      }

      function performOauth(onSuccess) {
        vm.onSuccess = onSuccess;
        //return vm.onSuccess();
        var url = buildAuthUrl();
        browserWindow = window.open(url, '_blank', 'location=no');
        browserWindow.addEventListener('loadstart', onLoadstart);
      }
    
      function onLoadstart(event) {
        if((event.url).startsWith('http://localhost/callback')) {
          requestToken = (event.url).split('code=')[1];
          var url = '...';
          var req = {
            method: 'POST',
            url: url,
            data: {
              'code': requestToken,
              'client_id': '...',
              'client_secret': '...',
              'redirect_uri': 'http://localhost/callback',
              'grant_type': 'authorization_code'
            }
          };
          return $http(req)
            .then(function(response) {
              /* jshint -W106*/
              storeInLocalStorage('access_token', response.data.access_token);
              browserWindow.close();
              vm.onSuccess();
            }, function(response) {
              $ionicPopup.alert({
                title: 'Error',
                template: 'Authentication failed.'
              });
              browserWindow.close();
            });
        }      
      }
    }
})();
