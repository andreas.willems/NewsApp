/* jshint -W117, -W030 */
describe('Test suite: Login controller', function() {
	var $controller;
	var $rootScope;
	var deferredLogin;
	var authenticationMock;
	var stateMock;
	var ionicPopupMock;

	// Load the module for the list
	beforeEach(module('app.login'));

	// Instantiate the controller and mocks for every test
	beforeEach(inject(function(_$controller_, $rootScope, $q) {

		deferredLogin = $q.defer();

		// mock Authentication service
		authenticationMock = {
			login: jasmine.createSpy('login spy')
				.and.returnValue(deferredLogin.promise)
		};



		// mock $state
		stateMock = jasmine.createSpyObj('$state spy', ['go']);

		// mock $ionicPopup
		ionicPopupMock = jasmine.createSpyObj('$ionicPopup spy', ['alert']);

		// initiate controller
		$controller = _$controller_('LoginController', {
			'$state': stateMock,
			'$scope': {},
			'$ionicPopup': ionicPopupMock,
      '$http': {},
			'Authentication': authenticationMock
		});
	}));

	describe('The controller', function() {

		it ('should exist', function() {
			expect($controller).toBeDefined();
		});

		it('should define a method login', function() {
			expect($controller.login).toBeDefined();
		});

		it('should define a method onSuccess', function() {
			expect($controller.onSuccess).toBeDefined();
		});

		it('should define a method onError', function() {
			expect($controller.onError).toBeDefined();
		});

	});

	describe('#login()', function() {
		it('should call login from the Authentication service',
			function() {
				$controller.login();
				expect(authenticationMock.login).toHaveBeenCalled();
			}
		);
	});

	describe('#onSuccess()', function() {
		it('should call $state.go with "app.listview"', function() {
			$controller.onSuccess();
			expect(stateMock.go).toHaveBeenCalledWith('app.listview');
		});
	});

	describe('#onError()', function() {
		it('should call $ionicPopup.alert', function() {
			$controller.onError();
			expect(ionicPopupMock.alert).toHaveBeenCalled();
		});
	});
});
