(function() {
  'use strict';

  angular
    .module('app.login')
    .controller('LoginController', LoginController);

  LoginController.$inject = ['$state', '$ionicPopup', 'Authentication'];

  /* @ngInject */
  function LoginController($state, $ionicPopup, Authentication) {
    var vm = this;

    vm.onSuccess = function onSuccess() {
      $state.go('app.listview');
    };

    vm.onError = function onError() {
      $ionicPopup.alert({
        title: 'Login failed',
        template: 'Could not perform authentication'
      });
    };

    vm.login = function login() {
      Authentication.login(vm.onSuccess);
    };

    return vm;
  }
})();
