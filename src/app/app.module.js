(function() {
  'use strict';

  angular
	.module('app', [
  	/* Shared modules */
	  'ionic',
	  'ngCordova',
	  //'ngIOS9UIWebViewPatch',
	  'app.core',
    'pascalprecht.translate',

    /* Feature areas */
	  'app.login',
    'app.menu',
	  'app.list',
	  'app.detail',
	  'app.logger'
	]);
})();
