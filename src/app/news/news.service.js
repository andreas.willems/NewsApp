(function() {
  'use strict';

  angular
    .module('news')
    .factory('News', News);

  News.$inject = ['$http', '$rootScope', '$window'];

  function News($http, $rootScope, $window) {
    var vm = this;
    vm.server = '...';
    //vm.server = 'http://192.168.1.50:8000';
    vm.apiPrefix = '/api/v1.0/...';
    vm.accessToken = $window.localStorage.getItem('access_token');
    vm.accessTokenHardCoded =
      '...';
    vm.format = '&_format=json';

    // return service interface
    return {
      getAllNews: getAllNews,
      getNewsById: getNewsById,
      getNewsFromPage: getNewsFromPage,
      getIndustriesForNewsWithId: getIndustriesForNewsWithId,
      getDomainsForNewsWithId: getDomainsForNewsWithId,
      getCategoryForNewsWithId: getCategoryForNewsWithId,
      sortMode: 'latestFirst',
      broadcast: broadcast,
      listen: listen
    };

    function getAllNews() {
      return makeGetRequestTo(
        vm.server + vm.apiPrefix + '?access_token=' + vm.accessToken
      );
    }

    function getNewsById(id) {
      return makeGetRequestTo(
        vm.server + vm.apiPrefix + '/' + id + '?access_token=' + vm.accessToken
      );
    }

    function getNewsFromPage(id) {
      return makeGetRequestTo(
        vm.server + vm.apiPrefix + '?access_token=' + vm.accessToken + '&page=' + id
      );
    }

    function getIndustriesForNewsWithId(id) {
      return makeGetRequestTo(
        vm.server + vm.apiPrefix + '/' + id + '/industries' +
          '?access_token=' + vm.accessToken
      );
    }

    function getDomainsForNewsWithId(id) {
      return makeGetRequestTo(
        vm.server + vm.apiPrefix + '/' + id + '/domains' +
          '?access_token=' + vm.accessToken
      );
    }

    function getCategoryForNewsWithId(id) {
      return makeGetRequestTo(
        vm.server + '/api/v1.0/category/' + id +
          '?access_token=' + vm.accessToken
      );
    }

    function makeGetRequestTo(url) {
      //console.log(url);
      //alert(url);
      return $http({
        method: 'GET',
        url: url
      });
    }

    function getAccessTokenFromLocalStorage() {
      vm.accessToken = window.localStorage['access_token'];
    }

    function broadcast(event) {
      $rootScope.$broadcast(event);
    }

    function listen(event, callback) {
      $rootScope.$on(event, callback);
    }
  }
})();
