/* jshint -W117, -W030 */
describe('Test suite: News service', function() {
  var News;
  var server;
  var apiPrefix;
  var accessToken;
  var format;

  // Load the module for the list
  beforeEach(module('news'));

  // Inject the service
  beforeEach(inject(function($injector) {
    News = $injector.get('News');
    server = '...';
    //server = 'http://192.168.1.50:8000';
    apiPrefix = '/api/v1.0/...';
    accessToken =
      '?access_token=...';
    format = '&_format=json';

    // inject $httpBackend service and setup mock routes
    $httpBackend = $injector.get('$httpBackend');

    $httpBackend
      .when('GET', server + apiPrefix + accessToken)
      .respond(
        {'page': 1, 'results': []}
      );

    $httpBackend
      .when('GET', server + apiPrefix+ '/101' + accessToken)
      .respond(
        {id: 101}
      );

    $httpBackend
      .when('GET', server + apiPrefix + accessToken + '&page=2')
      .respond(
        {'page': 2, 'results': []}
      );
  }));

  afterEach(function() {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('The service', function() {

    it('should exist', inject(function() {
      expect(News).toBeDefined();
    }));

    it('should define a method getAllNews', function() {
      expect(News.getAllNews).toBeDefined();
    });

    it('should define a method getNewsById', function() {
      expect(News.getNewsById).toBeDefined();
    });

    it('should define a method getNewsFromPage', function() {
      expect(News.getNewsFromPage).toBeDefined();
    });
  });

  describe('#getAllNews()', function() {
    it('should make a GET request', function() {
      $httpBackend.expectGET(server + apiPrefix + accessToken);
      News
        .getAllNews()
        .then(function(response) {
          expect(response.status).toBeDefined();
          expect(response.status).toBe(200);
          expect(response.data.results).toBeDefined();
          expect(Array.isArray(response.data.results)).toBeTruthy();
        }, function(error, status) {
          console.error(error);
          console.log(status);
        });
      $httpBackend.flush();
    });
  });

  describe('#getNewsById()', function() {
    it('should make a GET request', function() {
      $httpBackend.expectGET(server + apiPrefix + '/101' + accessToken);
      News
        .getNewsById(101)
        .then(function(response) {
          expect(response.status).toBeDefined();
          expect(response.status).toBe(200);
          expect(response.data).toBeDefined();
          expect(typeof response.data).toEqual('object');
        }, function(error, status) {
					console.error(error);
					console.log(status);
        });
      $httpBackend.flush();
    });
  });

  describe('#getNewsFromPage()', function() {
    it('should make a GET request', function() {
      $httpBackend.expectGET(server + apiPrefix + accessToken + '&page=2');
      News
        .getNewsFromPage(2)
        .then(function(response) {
          expect(response.status).toBeDefined();
          expect(response.status).toBe(200);
          expect(response.data).toBeDefined();
          expect(Array.isArray(response.data.results)).toBeTruthy();
        }, function(error, status) {
					console.error(error);
					console.log(status);
        });
      $httpBackend.flush();
    });
  });

});
