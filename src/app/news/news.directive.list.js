(function() {
  'use strict';

  angular
    .module('news')
    .directive('NewsList', NewsList);

  function NewsList() {
    return {
      restrict: 'AE',
      templateUrl: 'app/news/news.list.tpl.html',
      scope: {},
      link: link,
      controller: ListController,
      controllerAs: 'vm',
      bindToController: true
    };
  }

  function link(scope, element, attrs) {}

  ListController.$inject = ['News', '$ionicLoading', '$scope'];

  function ListController(News, $ionicLoading, $scope) {
    var vm = this;
    vm.news = [];
    vm.currentPage = 100;
    vm.nextPage = vm.currentPage - 1;
    vm.loaded = false;
    vm.connectionAvailable = true;
    vm.pageReset = false;
    vm.errorMessage = '';

    vm.activate = activate;
    vm.getNews = getNews;
    vm.updatePageCount = updatePageCount;
    vm.onSuccess = onSuccess;
    vm.onError = onError;
    vm.onFinally = onFinally;
    vm.resetPageCountAndGetNews = resetPageCountAndGetNews;

    News.listen('sortModeChanged', vm.resetPageCountAndGetNews);

    return vm;

    function activate() {
      vm.news = [];
      if (News.sortMode === 'latestFirst' ) {
        vm.currentPage = 100;
        vm.nextPage = vm.currentPage - 1;
      } else {
        vm.currentPage = 1;
        vm.nextPage = vm.currentPage + 1;
      }
    }

    function getNews() {
      News
        .getNewsFromPage(vm.currentPage)
        .then(onSuccess, onError)
        .finally(onFinally);
    }

    function updatePageCount() {
      vm.currentPage = vm.nextPage;
      if (News.sortMode === 'latestFirst') {
        vm.nextPage--;
      } else {
        vm.nextPage++;
      }
    }

    function onSuccess(response) {
      vm.news = vm.news.concat(response.data.results);
      vm.loaded = true;
      vm.connectionAvailable = true;
      vm.pageReset = false;
      updatePageCount();
    }

    function onError(response) {
      console.log(response);
      vm.loaded = false;
      vm.connectionAvailable = false;
      var errorText = response.status + '_' + JSON.stringify(response);
      var template = errorText +
        '<br/>Could not load news.</br>Please try again later.';
      vm.errorMessage = template;
      $ionicLoading.show({
        template: template,
        duration: 3000
      });

    }

    function getErrorText(statusCode) {
      var result = 'Client error';
      if (statusCode >= 500) {
        result = 'Server error ' + statusCode;
      }
      if (statusCode < 500 && statusCode >= 400) {
        result = 'Request error';
      }
      return result;
    }

    function onFinally() {
      $scope.$broadcast('scroll.refreshComplete');
      $scope.$broadcast('scroll.infiniteScrollComplete');
    }

    function resetPageCountAndGetNews() {
      vm.pageReset = true;
      vm.activate();
      vm.getNews();
    }
  }

})();
