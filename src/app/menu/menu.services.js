(function() {
  'use strict';
  
  angular
    .module('app.menu')
    .factory('Settings', Settings);
  
  Settings.$inject = ['$translate'];
  
  function Settings($translate) {
    var service = {
      setLanguage: setLanguage
    };
    return service;
    
    function setLanguage(key) {
      $translate.use(key);
    }
  }
  
})();