/* jshint -W117, -W030 */
describe('Test suit: Directive ionRadioFix', function() {
  var element;
  var scope;

  beforeEach(module('ionic'));

  beforeEach(inject(function($rootScope, $compile) {
    scope = $rootScope.$new();
    element = angular.element('<ion-radio-fix></ion-radio-fix>');
    $compile(element)(scope);
    scope.$digest();
  }));

  it('should exist', function() {
    expect(element).toBeDefined();
  });

});