/* jshint -W117, -W030 */
describe('Test suite: Menu controller', function() {
  var $controller;
  var scope;
  var authentication;

  // Load the module for the list
  beforeEach(module('app.menu'));

  // Instantiate the controller and mocks for every test
  beforeEach(inject(function($injector, _$controller_, $rootScope) {
    scope = $rootScope.$new();
    authentication = $injector.get('Authentication');

	$controller = _$controller_('MenuController', {
	  $scope: scope,
      Authentication: authentication
	});
  }));

  describe('The controller', function() {
    it ('should exist', function() {
	  expect($controller).toBeDefined();
	});

    it('should define a property settings.sortMode', function() {
      expect($controller.settings.sortMode).toBeDefined();
    });
  });
});