(function() {
	'use strict';

	angular.module('app.menu', [
		'ionic',
		'app.login',
		'app.list',
		'ngCordova',
		'pascalprecht.translate'
	]);
})();
