(function() {
  'use strict';

  angular
		.module('app.menu')
		.controller('MenuController', MenuController);

	MenuController.$inject = ['Authentication', 'News', '$ionicPopup',
                            '$cordovaToast', '$state', '$ionicHistory',
                            'Settings'];

	/* @ngInject */
	function MenuController(Authentication, News, $ionicPopup,
                          $cordovaToast, $state, $ionicHistory,
                          Settings) {
      var vm = this;
      vm.settings = {
        sortMode: News.sortMode,
        currentLanguage: 'de'
      };
      vm.updateSortMode = updateSortMode;
      vm.updateLanguage = updateLanguage;
      vm.showConfirm = showConfirm;
      return vm;

      function updateSortMode() {
		    News.sortMode = vm.settings.sortMode;
        News.broadcast('sortModeChanged');
      }

      function updateLanguage() {
        Settings.setLanguage(vm.settings.currentLanguage);
      }

      function showConfirm() {
        var confirmPopup = $ionicPopup.confirm({
          title: 'Logout',
          template: 'Are you sure you want to log out?'
        });
        confirmPopup.then(function(res) {
          if(res) {
            Authentication.logout(function() {
              $ionicHistory.clearHistory();
              $ionicHistory.clearCache();
              $ionicHistory.nextViewOptions({
                disableAnimate: true,
                disableBack: true
              });
              $state.go('login');
              $cordovaToast
                .showShortCenter('Logout successful')
                .then(function(success) {}, function(error) {});
            });
          } else {
            return;
          }
        });
      }
	}
})();
