# App

## Build

*   **Voraussetzungen**    
    Node.js (mit den Versionen 0.12.9, 4.2.3 und 5.1.1 getestet;  
    bei einem Wechsel der Version ist bei bestehenden Projekten häufig ein 
    "npm rebuild" notwendig).  
    Empfohlene Version: 5.1.1 (gefühlt am schnellsten)  
    Es sollte kein sudo erforderlich sein. node daher z.B. im Homeverzeichnis entpacken und
    in den PATH eintragen    
    ```Shell
    npm install -g gulp bower ionic cordova ios-sim
    ```
    
 
*   **Installation**  
    ```Shell
    git clone ...
    npm install
    bower install  
    ionic state reset  
    ionic resources --icon  
    ionic resources --splash  
    ```  
    
*   **Testen**  
    um die App im Simulator zu testen, folgende Befehle eingeben:  
    ```Shell
    ionic build [ios | android]
    ionic emulate [ios | android]
    ```